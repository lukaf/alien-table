package main

import (
	"fmt"
	"testing"

)

func TestWall(t *testing.T) {
	testCases := []struct{
		lane int
		expected wall
	}{
		{1, slab},
		{3, slab},
		{4, basic},
		{6, unclip},
		{20, unclip},
		{15, basic},
		{46, lead},
	}

	for i, tc := range testCases {
		t.Run(fmt.Sprintf("Wall test case %d", i), func(t *testing.T) {
			got := wallType(tc.lane)
			if got != tc.expected {
				t.Fatalf("Expected wall type '%s', got wall type '%s'\n", tc.expected, got)
			}
		})
	}
}
