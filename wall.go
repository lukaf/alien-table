package main

type wall string

const (
	slab   wall = "slab"
	lead        = "lead"
	unclip      = "unclip"
	basic       = "basic"
)

func wallType(lane int) wall {
	switch {
	case 1 <= lane && 3 >= lane:
		return slab
	case 5 <= lane && 10 >= lane || 19 <= lane && 21 >= lane:
		return unclip
	case lane >= 43 && lane <= 54:
		return lead
	default:
		return basic
	}
}
