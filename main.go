package main

import (
	"crypto/sha256"
	"encoding/csv"
	"encoding/hex"
	"html/template"
	"io"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
	"time"
)

const routeMaxDays = 365
const routeTimeFormat = "20060102"

type Route struct {
	Hash   string
	Added  string
	Colour string
	Lane   int
	Grade  string
	Type   wall
}

type Data struct {
	Routes []Route
	Date   string
}

func modDate(f *os.File) time.Time {
	stat, err := f.Stat()
	if err != nil {
		log.Printf("ERROR: unable to read routes file information: %s", err)
		return stat.ModTime().UTC()
	} else {
		return time.Now().UTC()
	}
}

func main() {
	f, err := os.Open("routes.csv")
	if err != nil {
		log.Fatal("unable to open csv")
	}
	defer f.Close()

	r := csv.NewReader(f)
	routes := make([]Route, 0)

	for {
		data, err := r.Read()
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatal(err)
		}

		if err := validate(data[0], dateValidator); err != nil {
			log.Fatalf("ERROR: invalid date value found '%s': %s", data, err)
		}

		if err := validate(data[1], colourValidator); err != nil {
			log.Fatalf("ERROR: invalid colour value found '%s': %s", data, err)
		}

		if err := validate(data[2], numberValidator); err != nil {
			log.Fatalf("ERROR: invalid lane value found '%s': %s", data, err)
		}

		if err := validate(data[3], gradeValidator); err != nil {
			log.Fatalf("ERROR: invalid grade value found '%s': %s", data, err)
		}

		line := strings.Join(data, "")

		lane, _ := strconv.Atoi(data[2])

		route := Route{
			Hash:   sum(line, 6),
			Added:  data[0],
			Colour: data[1],
			Lane:   lane,
			Grade:  data[3],
			Type:   wallType(lane),
		}

		if routeExists(route, routes) {
			log.Printf("ERROR: route '%s' already exists, skipping", line)
			continue
		}

		if oldRoute(route, routeMaxDays) {
			continue
		}

		routes = append(routes, route)
	}

	data := Data{
		Routes: routes,
		Date:   modDate(f).Format("2006-01-02T15:04:05"),
	}

	out, err := os.Create("public/index.html")
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()

	t := template.Must(template.ParseFiles("templates/index.html"))
	if err := t.Execute(out, data); err != nil {
		log.Fatal(err)
	}
}

func sum(data string, length int) string {
	h := sha256.New()
	h.Write([]byte(data))

	return hex.EncodeToString(h.Sum(nil))[:length]
}

func routeExists(route Route, routes []Route) bool {
	for _, r := range routes {
		if r.Hash == route.Hash {
			return true
		}
	}

	return false
}

func oldRoute(route Route, maxDays int) bool {
	created, err := time.Parse(routeTimeFormat, route.Added)
	if err != nil {
		log.Fatalf("ERROR: Route found with wrong date: %+v: %s", route, err)
	}

	return int(math.Floor(time.Now().Sub(created).Hours()/24)) > maxDays
}
