package main

import "testing"

func TestDateValidator(t *testing.T) {
	tc := []struct {
		input     string
		wantError bool
	}{
		{"20230201", false},
		{"20230102", false},
		{"20231221", false},
		{"20232112", true},
	}

	for _, c := range tc {
		expectedMsg := ""
		markedMsg := ""

		t.Run(c.input, func(t *testing.T) {
			err := dateValidator(c.input)
			gotError := err != nil

			if c.wantError {
				expectedMsg = "invalid"
			} else {
				expectedMsg = "valid"
			}

			if gotError {
				markedMsg = "invalid"
			} else {
				markedMsg = "valid"
			}

			if c.wantError != (err != nil) {
				t.Errorf("value %s expected to be %s date, but was marked as %s", c.input, expectedMsg, markedMsg)
			}
		})
	}
}

func TestStringValidator(t *testing.T) {
	if err := stringValidator(1); err == nil {
		t.Errorf("numeric value is not a string")
	}

	if err := stringValidator("string"); err != nil {
		t.Errorf("string not recognised as string")
	}
}

func TestNumberValidator(t *testing.T) {
	if err := numberValidator("number"); err == nil {
		t.Errorf("non numeric string should cause an error")
	}

	if err := numberValidator("123"); err != nil {
		t.Errorf("number string should not cause an error")
	}
}

func TestGradeValidator(t *testing.T) {
	tc := []struct {
		grade     string
		wantError bool
	}{
		{"5", false},
		{"5+", false},
		{"6a", false},
		{"6a+", false},
		{"6a-", true},
		{"6b+", false},
		{"6-", true},
		{"a", true},
		{"", true},
	}

	for _, c := range tc {
		wantMsg := ""
		gotMsg := ""

		t.Run(c.grade, func(t *testing.T) {
			// "grade %s is expected to be valid/invalid and was marked as valid/invalid"
			err := gradeValidator(c.grade)
			gotError := err != nil

			if c.wantError {
				wantMsg = "invalid"
			} else {
				wantMsg = "valid"
			}

			if gotError {
				gotMsg = "invalid"
			} else {
				gotMsg = "valid"
			}

			if c.wantError != gotError {
				t.Errorf("grade %s is expected to be %s, but was marked as %s", c.grade, wantMsg, gotMsg)
			}

		})
	}
}

func TestColourValidator(t *testing.T) {
	tc := []struct {
		colour    string
		wantError bool
	}{
		{"red", false},
		{"blue", false},
		{"blue swirlies", false},
		{"lava lamp", false},
		{"pockets + jams", false},
		{"black + fff + pockets for feet", false},
		{"lava lamps", true},
		{"burgundy", true},
	}

	for _, c := range tc {
		wantMsg := ""
		gotMsg := ""

		t.Run(c.colour, func(t *testing.T) {
			err := colourValidator(c.colour)
			gotError := err != nil

			if c.wantError {
				wantMsg = "invalid"
			} else {
				wantMsg = "valid"
			}

			if gotError {
				gotMsg = "invalid"
			} else {
				gotMsg = "valid"
			}

			if c.wantError != gotError {
				t.Errorf("colour '%s' is expected to be %s, but was marked as %s", c.colour, wantMsg, gotMsg)
			}

		})
	}
}
