#!/bin/bash

set -euo pipefail

mkdir -p public/js

cp js/*.js public/js/
go run .
