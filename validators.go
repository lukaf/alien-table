package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

type validator func(interface{}) error

func validate(value string, f validator) error {
	return f(value)
}

func dateValidator(value interface{}) error {
	if err := numberValidator(value); err != nil {
		return err
	}

	_, err := time.Parse(routeTimeFormat, value.(string))
	return err
}

func stringValidator(value interface{}) error {
	_, ok := value.(string)
	if !ok {
		return errors.New("value is not a string")
	}

	return nil
}

func numberValidator(value interface{}) error {
	if err := stringValidator(value); err != nil {
		return err
	}

	_, err := strconv.Atoi(value.(string))
	return err
}

func gradeValidator(value interface{}) error {
	validNumbers := []string{"4", "5", "6", "7", "8", "9"}
	validLetters := []string{"a", "b", "c"}
	validSuffixes := []string{"+"}

	if err := stringValidator(value); err != nil {
		return err
	}

	tokens := strings.Split(value.(string), "")
	if len(tokens) == 0 || len(tokens) > 3 {
		return errors.New("grade must be a string with 1 to 3 characters")
	}

	if _, err := strconv.Atoi(tokens[0]); err != nil {
		return errors.New("grade's first character must be a number")
	}

	if !contains(tokens[0], validNumbers) {
		return fmt.Errorf("grade number should be one of: %s", strings.Join(validNumbers, ","))
	}

	if len(tokens) >= 2 {
		validChars := append(validLetters, validSuffixes...)
		if !contains(tokens[1], validChars) {
			return fmt.Errorf("grade's second characted should be one of: %s", strings.Join(validChars, ","))
		}
	}

	if len(tokens) == 3 {
		if !contains(tokens[2], validSuffixes) {
			return fmt.Errorf("grade suffix should be one of: %s", strings.Join(validSuffixes, ","))
		}
	}

	return nil
}

func colourValidator(value interface{}) error {
	validColourComponents := []string{
		"Alex Honnold",
		"arete",
		"arete for feet",
		"arete for hands",
		"black spots",
		"black",
		"blobs for feet",
		"blobs",
		"blood orange",
		"blue swirlies",
		"blue",
		"blue for feet",
		"bridging",
		"crack",
		"door",
		"features",
		"fff",
		"flake",
		"finger crack",
		"finger crack for feet",
		"green",
		"grey",
		"jams",
		"lava lamp",
		"ledge",
		"lilac",
		"mint",
		"no arete",
		"no bridging",
		"no features",
		"no fff",
		"orange",
		"pink",
		"pockets for feet",
		"pockets",
		"purple",
		"purple for hands",
		"rainbow",
		"red spots",
		"red",
		"to top of roof",
		"tufa",
		"white",
		"woodies",
		"yellow",
	}

	if err := stringValidator(value); err != nil {
		return err
	}

	colour := value.(string)

	colourComponents := strings.Split(colour, "+")

	for _, component := range colourComponents {
		component = strings.TrimSpace(component)

		if !contains(component, validColourComponents) && occurences(component, validColourComponents) != 1 {
			return fmt.Errorf("'%s' is not a valid colour", colour)
		}
	}

	return nil
}

func contains[T comparable](element T, collection []T) bool {
	for _, item := range collection {
		if item == element {
			return true
		}
	}

	return false
}

func occurences[T comparable](element T, collection []T) int {
	count := 0
	for _, item := range collection {
		if item == element {
			count++
		}
	}

	return count
}
