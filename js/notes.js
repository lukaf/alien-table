if (localStorageAvailable()) {
  let existingData = [];

  table.on("cellEdited", (cell) => {
    
    const newData = table
          .getEditedCells()
          .map((cell) => cell.getData())
    // extract only needed data
          .map(({id, note}) => ({id, note}));
    
    const newDataIds = newData.map((item) => item.id);

    const merged = existingData
          .filter((item) => !newDataIds.includes(item.id))
          .concat(newData)
    // skip deleted notes aka save notes with content
          .filter((item) => item.note);

    localStorage.setItem("tabulator-alien-table-notes",JSON.stringify(merged));
  });

  table.on("tableBuilt", () => {
    const data = localStorage.getItem("tabulator-alien-table-notes");

    if (data) {
      existingData = JSON.parse(data);
      table.updateData(existingData);

      // refresh filter to show updated rows
      let notesFilter = table.getHeaderFilterValue("note");
      if (notesFilter) {
        table.setHeaderFilterValue("note", null);
        table.setHeaderFilterValue("note", notesFilter);
      }
    }
  });
}
