const ageInMonths = (added) => {
  let start = luxon.DateTime.fromFormat(added, "yyyyMMdd");
  let end = luxon.DateTime.now();

  return Math.floor(end.diff(start, "months").toObject().months);
};

const localStorageAvailable = () => {
  let storage;
  try {
    storage = window["localStorage"];
    const x = "__storage_test__";
    storage.setItem(x, x);
    storage.removeItem(x, x);
    return true;
  } catch(e) {
    return false;
  }
};


const gradeFilterFunc = (value, row) => {
  const grade = value.replace(/ /g, "").replace(/,/g, "|").replace("+", "\\+");

  try {
    const regex = new RegExp(`^(${grade})$`);
    return regex.test(row);
  } catch (e) {
    return false;
  }
};
