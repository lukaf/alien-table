package main

import (
	"fmt"
	"time"
	"testing"
)

func TestOldRoute(t *testing.T) {
	testCases := []struct{
		ageInDays int
		maxDays int
		old bool
	}{
		{1, 10, false},
		{20, 10, true},
	}

	now := time.Now().UTC()

	for i, tc := range testCases {
		route := Route{
			Hash: "testing",
			Colour: "testing",
			Lane: 1,
			Grade: "testing",
			Added: now.Add(-(time.Hour * 24 * time.Duration(tc.ageInDays))).Format(routeTimeFormat),
		}

		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			if oldRoute(route, tc.maxDays) != tc.old {
				t.Errorf("Route %+v old status expected to be %t", route, tc.old)
			}
		})
	}

}
